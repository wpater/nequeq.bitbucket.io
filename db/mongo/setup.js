// Run: mongo localhost:27017/portfolio path to setup.js

conn = new Mongo("127.0.0.1");
db = conn.getDB("portfolio");

// Clean db
db.projects.drop();

// Add projects
// SYMWEB
db.projects.insert({
    "name": "SYMWEB",
    "shortName": "websym",
    "summary": "Web application for running simulations on remote server.",
    "description": "The main purpose of SYMWEB project was to create a web application which allows user to start " +
    "simulations in eVolutus environment on remote server. Other functionalities were: saving sets of configurations" +
    " files which are needed for starting simulation, updating progress for started simulation and sending brief" +
    " report for simulation to user via email. eVolutus simulator and database with results of simulations are" +
    " independent systems for which SYMWEB application is a common and consistent interface for users." +
    " Suitable for mobile devices too.<br/>" +
    "In the final version, SYMWEB application offers:<ul>" +
    "<li>start simulation on remote server with predefined set of configuration files</li>" +
    "<li>watch progress of started simulation</li>" +
    "<li>create reports for simulation</li>" +
    "<li>add/edit/delete set of configuration files</li>" +
    "<li>add tags to simulation/set of configuration files</li>" +
    "<li>add comments to simulation</li>" +
    "<li>choose remote server for simulation</li></ul>" +
    "Except main functionalities, some smaller ones were added:<ul>" +
    "<li>automatic reports from remote servers</li>" +
    "<li>views for mobile devices</li>" +
    "<li>dynamically refresh progress of simulation</li></ul>" +
    "Additionally, as result of project, documentation set were created.<br/>" +
    "This project is finished.<br/>" +
    "Team: Wojciech Pater & Piotr Stawicki",
    "images":
        [
            {
                "path": "/resources/img/symweb/login_page.png",
                "description": "Here user can log in or be redirected to register view.",
                "name": "Login page"
            },
            {
                "path": "/resources/img/symweb/config.png",
                "description": "List of uploaded configs for user. Dynamic search found one config with '27'. Below table in tabs this config is loaded and can be edited.",
                "name": "List of configs"
            },
            {
                "path": "/resources/img/symweb/list_of_simulations.png",
                "description": "View with simulation status. 3 links to generated charts and output received from simulator.",
                "name": "Simulation status"
            },
            {
                "path": "/resources/img/symweb/simulation_progress.png",
                "description": "List of all simulations created by user.",
                "name": "User simulations"
            }
        ],
    "tags":
        [
            "Java", "Java Spring", "Spring Security", "SQLite", "Apache Tomcat 8", "Bootstrap", "MyBatis", "JUnit",
            "JSTL", "JavaScript", "git"
        ],
    "repo": "https://bitbucket.org/wpater/websym"
});

// missmed
db.projects.insert({
    "name": "MiSS SymbolicMed",
    "shortName": "miss",
    "summary": "Generation of pharmacokinetic models based on AgE Framework.",
    "description": "Generation of pharmacokinetic models based on AgE Framework.<br/>" +
    "The main purpose of this project was to generate functions for given dataset using symbolic regression" +
    " and genetic programming.<br/>" +
    "Generated functions can be used by pharmacists for preparating models for new medicaments." +
    " Test with simple functions like x*sin(x) or xin(x) + sin(y) are promising. In many cases (especially for" +
    " functions with one variable) application generated original function. Elements:<ul>" +
    "<li>adding</li>" +
    "<li>subtraction</li>" +
    "<li>multiplication</li>" +
    "<li>division</li>" +
    "<li>sinus</li>" +
    "<li>cosinus</li>" +
    "<li>natural logarithm</li>" +
    "<li>exponentiation</li>" +
    "<li>numbers</li>" +
    "<li>variables</li></ul>" +
    "Check report for more information and examples (in Polish).",
    "images":
        [
            {
                "path": "../resources/img/missmed/xsinx1.png",
                "description": "Target x*sin(x) - grey",
                "name": "Plot x*sin(sin(x))"
            },
            {
                "path": "../resources/img/missmed/xsinx2.png",
                "description": "Target x*sin(x) - grey",
                "name": "Plot x*(sin(x)+ln(0.94838))"
            }
        ],
    "tags":
        [
            "Java", "Spring", "AgE", "Gradle"
        ],
    "repo": "https://bitbucket.org/wpater/miss",
    "additional_links":
        {"Report": "../resources/img/missmed/miss-symbolicmed.pdf"}
});

// Ulam
db.projects.insert({
    "name": "Ulam on GPU",
    "shortName": "ulam",
    "summary": "Big numbers library for GPU applied in cryptography.",
    "description": "This is my master thesis.<br/>" +
    "Library written in C++ for arithmetic operations on Big Numbers in CPU and GPU environment.<br/>" +
    "Supports:<ul>" +
    "<li>adding</li>" +
    "<li>subtraction</li>" +
    "<li>multiplication</li>" +
    "<li>division</li>" +
    "<li>mod</li>" +
    "<li>sqrt</li></ul>" +
    "An additional module 'ulam' is uded for prime factorization on GPU (see links).<br/>" +
    "This project is being developed.",
    "images": [],
    "tags":
        [
            "C++", "CUDA", "Makefile", "git", "Python", "TDD"
        ],
    "repo": "https://bitbucket.org/wpater/ulam",
    "additional_links":
        {
            "MSc thesis (in Polish)": "../resources/img/ulam/Praca_Magisterska.pdf",
            "Short abstract (in English)": "../resources/img/ulam/abstract.pdf"
        }

});

// Portfolio
db.projects.insert({
    "name": "Portfolio",
    "shortName": "portfolio",
    "summary": "Web application as my personal online portfolio.",
    "description": "This is place where I'm experimenting with Java, interesting for me frameworks and other technologies.<br/>" +
    "For example I'm using here Postgres and MongoDB - I know that's overkill in this case but I just wanted to try this.<br/>" +
    "Frontend is created with JSTL and the newest Bootstrap 4.<br/>" +
    "For backend I used Java 8 with Spring and Hibernate, for logging - log4j.<br/>" +
    "Everything is built by Maven and run with Spring Boot (Apache Tomcat).",
    "images": [],
    "tags":
        [
            "Java", "Spring", "Bootstrap", "Maven", "Bitbucket", "Hibernate", "Postgresql", "MongoDB",
            "JSTL", "git", "log4j", "Docker", "nginx"
        ],
    "repo": "https://bitbucket.org/wpater/nequeq.bitbucket.io"
});

// Flatmanager
// db.projects.insert({
//     "name": "FlatManager",
//     "shortName": "flatmanager",
//     "summary": "Web application for managing flat.",
//     "description": "It's simple application - main purpose is get familiar with micro services, docker etc. We are just started with it.",
//     "images": [
//         {
//             "path": "/resources/img/flatmanager/architecture.png",
//             "description": "First draft of architecture for this application.",
//             "name": "Architecture"
//         }
//     ],
//     "tags":
//         [
//             "Java", "Spring", "Bootstrap", "Maven", "Bitbucket", "MongoDB", "Docker", "Microservices",
//             "Angular", "git", "Jenkins", "Lombok"
//         ]
// });

DROP TABLE IF EXISTS skill;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS rate;

CREATE TABLE public.rate
(
    id SERIAL PRIMARY KEY NOT NULL,
    points INT NOT NULL,
    description TEXT NOT NULL
);
CREATE UNIQUE INDEX rate_id_uindex ON public.rate (id);

CREATE TABLE public.category
(
  id SERIAL PRIMARY KEY NOT NULL,
  name TEXT NOT NULL
);
CREATE UNIQUE INDEX category_id_uindex ON public.category (id);
CREATE UNIQUE INDEX category_name_uindex ON public.category (name);

CREATE TABLE public.skill
(
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    rate_id INT REFERENCES rate (id),
    experience INT NOT NULL,
    commercial BOOLEAN NOT NULL,
    category_id INTEGER REFERENCES category (id)
);
CREATE UNIQUE INDEX skill_id_uindex ON public.skill (id);
CREATE UNIQUE INDEX skill_name_uindex ON public.skill (name);


INSERT INTO rate (points, description) VALUES (1, 'Hello world level');
INSERT INTO rate (points, description) VALUES (2, 'Sometimes little help is needed');
INSERT INTO rate (points, description) VALUES (3, 'I can resolve problems without help. Individual research is enough');
INSERT INTO rate (points, description) VALUES (4, 'I do not have problems with working in this. Sometimes I can help');
INSERT INTO rate (points, description) VALUES (5, 'Expert level. I can teach other people');

INSERT INTO category (name) VALUES ('Languages');
INSERT INTO category (name) VALUES ('Tools');
INSERT INTO category (name) VALUES ('Frameworks');
INSERT INTO category (name) VALUES ('Other');

INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 2, TRUE, 'Erlang', 1);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 6, TRUE, 'Java', 1);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (2, 6, FALSE, 'C/C++', 1);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (2, 4, FALSE, 'Python', 1);

INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 5, TRUE, 'Git', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 3, TRUE, 'Jenkins', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 3, TRUE, 'Gerrit', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 2, TRUE, 'JIRA', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 5, FALSE, 'Bitbucket', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 3, TRUE, 'Maven', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 5, TRUE, 'Eclipse', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 4, TRUE, 'IntelliJ', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 2, TRUE, 'Docker', 2);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 3, TRUE, 'nginx', 2);

INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 2, TRUE, 'Common Test (Erlang)', 3);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 3, FALSE, 'Spring (Java)', 3);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 4, TRUE, 'Twitter Bootstrap (HTML/CSS/JS)', 3);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 1, TRUE, 'Django CMS (Python)', 3);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (2, 1, FALSE, 'CUDA (C++)', 3);

INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (5, 5, TRUE, 'Troubleshooting', 4);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (5, 4, TRUE, 'Code review', 4);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (4, 2, TRUE, 'Moderator - retrospectives and meetings', 4);
INSERT INTO skill (rate_id, experience, commercial, name, category_id) VALUES (3, 3, TRUE, 'Kanban', 4);
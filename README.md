# README #

My Portfolio

# Run #

``` mvn clean install dockerfile:build ```

``` docker-compose up -d ```


# Tech stack #

* Java 8
* Spring
* JSP
* Mongo DB
* Postgres
* nginx


# Last updates #

* Moved to Java 11
* Moved to Docker - databases and web app
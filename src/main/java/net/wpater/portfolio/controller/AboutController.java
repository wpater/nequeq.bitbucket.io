package net.wpater.portfolio.controller;

import net.wpater.portfolio.model.postgres.Category;
import net.wpater.portfolio.model.postgres.Rate;
import net.wpater.portfolio.model.postgres.Skill;
import net.wpater.portfolio.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by wpater on 8/4/17.
 */

@Controller
public class AboutController {

    @Autowired
    private SkillService skillService;

    @RequestMapping("/about")
    public String about(Model model) {
        List<Rate> rates = skillService.getAllRates();
        Collections.sort(rates);
        Collections.reverse(rates);
        List<Skill> skills = skillService.getAllSkills();
        Collections.sort(skills);
        Collections.reverse(skills);
        List<Category> categories = skillService.getAllCategories();
        model.addAttribute("rates", rates);
        model.addAttribute("skills", skills);
        model.addAttribute("categories", categories);
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        return "about";
    }
}

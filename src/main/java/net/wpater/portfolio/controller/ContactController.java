package net.wpater.portfolio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;

/**
 * Created by wpater on 8/4/17.
 */

@Controller
public class ContactController {

    @RequestMapping("/contact")
    public String contact(Model model) {
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        return "contact";
    }
}

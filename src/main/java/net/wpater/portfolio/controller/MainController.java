package net.wpater.portfolio.controller;

import net.wpater.portfolio.model.mongo.Project;
import net.wpater.portfolio.service.ProjectService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.Calendar;
import java.util.List;

/**
 * Created by wpater on 7/28/17.
 */

@Controller
public class MainController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/")
    public String index(Model model) {
        List<Project> list = projectService.getThreeLastUpdated();
        model.addAttribute("projects", list);
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        return "index";
    }
}

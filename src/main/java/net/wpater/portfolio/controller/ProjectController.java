package net.wpater.portfolio.controller;

import net.wpater.portfolio.model.mongo.Project;
import net.wpater.portfolio.service.ProjectService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wpater on 8/4/17.
 */

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/projects")
    public String projects(Model model) {
        List<List<Project>> list = projectService.formatProjectsToThreeColumns();
        model.addAttribute("projects", list);
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        model.addAttribute("tags", "");
        return "projects";
    }

    @RequestMapping("/projects/{shortName}")
    public String ulam(Model model, @PathVariable("shortName") String shortName) {
        Project project = projectService.findByShortName(shortName);
        model.addAttribute("project", project);
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        return "project";
    }

    @RequestMapping(value = "/projects", method = RequestMethod.POST)
    public String findProjects(Model model, @RequestParam String tags) {
        List<List<Project>> list;
        if (!tags.equals(null) && !tags.equals(""))
            list = projectService.findByTagsAndFormat(Arrays.asList(tags.split(",")));
        else list= projectService.formatProjectsToThreeColumns();
        model.addAttribute("projects", list);
        model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
        model.addAttribute("tags", tags);
        return "projects";
    }
}

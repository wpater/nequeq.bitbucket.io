package net.wpater.portfolio.service;

import net.wpater.portfolio.model.mongo.Project;

import java.util.List;

public interface ProjectService {

    List<Project> findAll();
    List<Project> findAllUpdated();
    List<Project> getThreeLastUpdated();
    List<List<Project>> formatProjectsToThreeColumns();
    Project findByName(String name);
    Project findByShortName(String name);
    List<List<Project>> findByTagsAndFormat(List<String> tags);
}

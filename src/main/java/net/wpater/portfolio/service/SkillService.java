package net.wpater.portfolio.service;

import net.wpater.portfolio.model.postgres.Category;
import net.wpater.portfolio.model.postgres.Rate;
import net.wpater.portfolio.model.postgres.Skill;

import java.util.List;

public interface SkillService {

    List<Skill> getAllSkills();
    List<Rate> getAllRates();
    List<Category> getAllCategories();
}

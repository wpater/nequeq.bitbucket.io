package net.wpater.portfolio.service.impl;

import net.wpater.portfolio.dao.CategoryDao;
import net.wpater.portfolio.dao.RateDao;
import net.wpater.portfolio.dao.SkillDao;
import net.wpater.portfolio.model.postgres.Category;
import net.wpater.portfolio.model.postgres.Rate;
import net.wpater.portfolio.model.postgres.Skill;
import net.wpater.portfolio.service.SkillService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("skillService")
@Transactional
public class SkillServiceImpl implements SkillService {

    private static final Logger LOGGER = Logger.getLogger(SkillServiceImpl.class);

    @Autowired
    private SkillDao skillDao;
    @Autowired
    private RateDao rateDao;
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public List<Skill> getAllSkills() {
        List<Skill> list = skillDao.findAll();
        LOGGER.debug("All skills: " + list);
        return list;
    }

    @Override
    public List<Rate> getAllRates() {
        List<Rate> list = rateDao.findAll();
        LOGGER.debug("All rates: " + list);
        return list;
    }

    @Override
    public List<Category> getAllCategories() {
        List<Category> list = categoryDao.findAll();
        LOGGER.debug("All categories: " + list);
        return list;
    }
}

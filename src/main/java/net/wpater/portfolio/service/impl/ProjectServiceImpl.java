package net.wpater.portfolio.service.impl;

import net.wpater.portfolio.model.mongo.Project;
import net.wpater.portfolio.service.ProjectService;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;


@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOGGER = Logger.getLogger(SkillServiceImpl.class);

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public List<Project> findAll() {
        List<Project> list = mongoOperations.findAll(Project.class);
        LOGGER.debug("All projects: " + list);
        return list;
    }

    @Override
    public List<Project> findAllUpdated() {
        List<Project> list = findAll();
        for (Project item : list) {
            RestTemplate restTemplate = new RestTemplate();
            String link = item.getRepo().replaceAll("https://bitbucket.org/", "");
            String repoURL = "https://api.bitbucket.org/2.0/repositories/" + link;
            LOGGER.debug(repoURL);
            JSONObject json = restTemplate.getForObject(repoURL, JSONObject.class);
            item.setUpdated(json.get("updated_on").toString().substring(0, 10).replace('-', '/'));
        }
        return list;
    }

    @Override
    public List<Project> getThreeLastUpdated() {
        List<Project> list = findAllUpdated();
        Collections.sort(list);
        Collections.reverse(list);
        return list.subList(0, 3);
    }

    @Override
    public List<List<Project>> formatProjectsToThreeColumns() {
        return formatList(findAllUpdated());
    }

    @Override
    public Project findByName(String name) {
        Query query = new Query(Criteria.where("name").is(name));
        Project project = mongoOperations.findOne(query, Project.class);
        LOGGER.debug("Found project: " + project);
        return project;
    }

    @Override
    public Project findByShortName(String name) {
        Query query = new Query(Criteria.where("shortName").is(name));
        Project project = mongoOperations.findOne(query, Project.class);
        LOGGER.debug("Found project: " + project);
        return project;
    }

    @Override
    public List<List<Project>> findByTagsAndFormat(List<String> tags) {
        List<Project> allProjects = findAllUpdated();
        List<Project> list = new LinkedList<>();
        allProjects.forEach(item ->
                tags.forEach(findTag -> {
                    if (item.getTags().stream().anyMatch(tag ->
                            tag.toLowerCase().equals(findTag.toLowerCase()))) list.add(item);
                })
        );
        return formatList(list);
    }

    private List<List<Project>> formatList(List<Project> list) {
        Collections.sort(list);
        Collections.reverse(list);
        List<List<Project>> preparedList = new LinkedList<>();
        int iterator = 0;
        List<Project> row = new LinkedList<>();
        int max = list.size();
        if (max % 3 == 1 && max > 2) max -= 4;
        else if (max % 3 == 2 && max > 2) max -= 2;
        for (int i = 0; i < max; i++) {
            if (iterator < 3) {
                iterator++;
            } else {
                preparedList.add(row);
                iterator = 0;
                row = new LinkedList<>();
            }
            row.add(list.get(i));
        }
        for (int i = max; i < list.size(); i++) {
            if (iterator < 2) {
                iterator++;
            } else {
                preparedList.add(row);
                iterator = 0;
                row = new LinkedList<>();
            }
            row.add(list.get(i));
        }
        // Add last row (not full)
        preparedList.add(row);
        return preparedList;
    }

}

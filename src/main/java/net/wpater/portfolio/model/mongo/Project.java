package net.wpater.portfolio.model.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document(collection = "projects")
public class Project implements Comparable<Project> {

    @Id
    private String id;

    private String name;

    private String shortName;

    private String summary;

    private String description;

    private List<Image> images;

    private List<String> tags;

    private String repo;

    private String updated;

    private Map<String, String> additional_links;

    public Project() {}

    public Project(String id, String name, String shortName, String summary, String description, List<Image> images, List<String> tags, String repo, String updated, Map<String, String> additional_links) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.summary = summary;
        this.description = description;
        this.images = images;
        this.tags = tags;
        this.repo = repo;
        this.updated = updated;
        this.additional_links = additional_links;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", summary='" + summary + '\'' +
                ", description='" + description + '\'' +
                ", images=" + images +
                ", tags=" + tags +
                ", repo='" + repo + '\'' +
                ", updated='" + updated + '\'' +
                ", additional_links=" + additional_links +
                '}';
    }

    public Map<String, String> getAdditional_links() {
        return additional_links;
    }

    public void setAdditional_links(Map<String, String> additional_links) {
        this.additional_links = additional_links;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getRepo() {
        return repo;
    }

    public void setRepo(String repo) {
        this.repo = repo;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int compareTo(Project project) {
        return this.updated.compareTo(project.getUpdated());
    }
}

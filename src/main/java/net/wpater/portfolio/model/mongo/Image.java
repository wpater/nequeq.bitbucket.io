package net.wpater.portfolio.model.mongo;

public class Image {

    private String path;

    private String description;

    private String name;

    public Image() {}

    public Image(String path, String description, String name) {
        this.path = path;
        this.description = description;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Image{" +
                "path='" + path + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

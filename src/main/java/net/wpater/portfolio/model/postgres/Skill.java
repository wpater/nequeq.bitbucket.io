package net.wpater.portfolio.model.postgres;

import javax.persistence.*;

@Entity
@Table
public class Skill implements Comparable<Skill> {

    @Id
    private Integer id;

    @Column(nullable=false)
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    private Rate rate;

    @Column(nullable=false)
    private Integer experience;

    @Column(nullable=false)
    private boolean commercial;

    @OneToOne(cascade = CascadeType.ALL)
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public boolean isCommercial() {
        return commercial;
    }

    public void setCommercial(boolean commercial) {
        this.commercial = commercial;
    }

    @Override
    public int compareTo(Skill o) {
        return this.rate.compareTo(o.rate);
    }
}

package net.wpater.portfolio.dao;

import net.wpater.portfolio.model.postgres.Category;

import java.util.List;

public interface CategoryDao {

    List<Category> findAll();
    Category findById(Integer id);
}

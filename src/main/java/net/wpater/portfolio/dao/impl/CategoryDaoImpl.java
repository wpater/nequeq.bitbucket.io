package net.wpater.portfolio.dao.impl;

import net.wpater.portfolio.dao.CategoryDao;
import net.wpater.portfolio.model.postgres.Category;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDaoImpl implements CategoryDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Category> findAll() {
        return (List<Category>) sessionFactory.getCurrentSession().createQuery("from Category").list();
    }

    @Override
    public Category findById(Integer id) {
        return sessionFactory.getCurrentSession().get(Category.class, id);
    }
}

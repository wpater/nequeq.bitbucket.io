package net.wpater.portfolio.dao.impl;

import net.wpater.portfolio.dao.SkillDao;
import net.wpater.portfolio.model.postgres.Skill;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SkillDaoImpl implements SkillDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Skill> findAll() {
        return (List<Skill>) sessionFactory.getCurrentSession().createQuery("from Skill").list();
    }

    @Override
    public Skill getById(Integer id) {
        return sessionFactory.getCurrentSession().get(Skill.class, id);
    }
}

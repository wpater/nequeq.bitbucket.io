package net.wpater.portfolio.dao.impl;

import net.wpater.portfolio.dao.RateDao;
import net.wpater.portfolio.model.postgres.Rate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RateDaoImpl implements RateDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Rate> findAll() {
        return (List<Rate>) sessionFactory.getCurrentSession().createQuery("from Rate").list();
    }

    @Override
    public Rate findById(Integer id) {
        return sessionFactory.getCurrentSession().get(Rate.class, id);
    }
}

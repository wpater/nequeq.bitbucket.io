package net.wpater.portfolio.dao;

import net.wpater.portfolio.model.postgres.Skill;

import java.util.List;

public interface SkillDao {

    List<Skill> findAll();
    Skill getById(Integer id);
}

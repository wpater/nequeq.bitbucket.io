package net.wpater.portfolio.dao;

import net.wpater.portfolio.model.postgres.Rate;

import java.util.List;

public interface RateDao {

    List<Rate> findAll();
    Rate findById(Integer id);
}

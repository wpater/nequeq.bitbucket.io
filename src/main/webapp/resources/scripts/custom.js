$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.collapse').collapse();
})

function repoUpdated(date) {
    var today = new Date();
    var past = new Date(date);
    var timeDiff = Math.abs(past.getTime() - today.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    var string = "";
    if (diffDays == 1) {
        string += diffDays + " day";
    } else {
        string += diffDays + " days";
    }
    return string;
}

function workInActualCorp() {
    var today = new Date();
    var past = new Date(2018, 08, 01);
    var diff = new Date(today - past);
    var years = diff.getUTCFullYear() - 1970;
    var months = diff.getMonth();
    var string = " (";
    switch (years + "-" + months) {
        case "0-0":
            string += "first month).";
            break;
        case "0-1":
            string += "1 month).";
            break;
        case "0-" + months:
            string += months + " months).";
            break;
        case "1-0":
            string += "1 year).";
            break;
        case "1-1":
            string += "1 year and 1 month).";
            break;
        case "1-" + months:
            string += "1 year and " + months + " months).";
            break;
        case years + "-0":
            string += years + " years).";
            break;
        case years + "-1":
            string += years + " years and 1 month).";
            break;
        default:
            string += years + " years and " + months + " months).";
            break;
    }
    return string;
}

document.getElementById("dateDiff").innerHTML = workInActualCorp();

<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/favicon.ico"/>"/>
<!-- Bootstrap -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
<!-- Main CSS -->
<link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet">
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/f56d6efb92.js"></script>

<script src="/resources/scripts/custom.js"></script>
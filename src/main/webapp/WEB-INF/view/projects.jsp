<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Wojciech Pater - Contact</title>
    <%@include file="../templates/includes.jsp" %>
</head>

<body>
<!-- Menu -->
<%@include file="../templates/menu.jsp" %>
<!-- Content -->
<div class="container lower-container">

    <div class="row">
        <div class="col-lg-12">
            <label for="tags">Are you looking for something special? Try to add tags below:</label>
            <form:form action="/projects" method="POST">
                <div class="input-group">
                    <input type="text" class="form-control" id="tags" name="tags"
                           placeholder="Separate tags by semicolon" value="${tags.equals("") ? "" : tags}">
                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit">Search</button>
                </span>
                </div>
            </form:form>
        </div>
    </div>

    <hr>
    <c:forEach var="row" items="${projects}">
        <div class="card-deck mb-3">
            <c:forEach var="item" items="${row}">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">${item.name}</h4>
                        <p class="card-text">${item.summary}</p>
                        <p class="card-text">
                            <a href="/projects/${item.shortName}" class="btn btn-primary">Description &raquo;</a>
                            <a target="_blank" href="${item.repo}" class="btn btn-primary">Repo &raquo;</a>
                        </p>
                        <p class="card-text">
                            <c:forEach var="tag" items="${item.tags}">
                                <code>${tag}</code>
                            </c:forEach>
                        </p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Last updated
                            <script>document.write(repoUpdated("${item.updated}"));</script>
                            ago
                        </small>
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:forEach>

    <nav class="breadcrumb mt-4">
        <a class="breadcrumb-item" href="/">Home</a>
        <span class="breadcrumb-item active">My projects</span>
    </nav>
    <%@include file="../templates/footer.jsp" %>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>

</html>
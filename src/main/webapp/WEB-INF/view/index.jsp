<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Wojciech Pater</title>
    <%@include file="../templates/includes.jsp" %>
</head>

<body>
<!-- Menu -->
<%@include file="../templates/menu.jsp" %>
<!-- Content -->
<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">Hello world!</h1>
        <p class="lead">Welcome on my personal portfolio.</p>
        <hr>
        <p>I'm a programmer from Cracow, Poland. I work mainly with Java, Erlang, sometimes with C++ and Python.</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom"
               title="Take a brief look into my projects" href="/projects" role="button">Check my projects &raquo;</a>
            <a class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom"
               title="More information about me" href="/about" role="button">About me &raquo;</a>
            <a class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom"
               title="And hire me!" href="/contact" role="button">Contact me! &raquo;</a>
        </p>
    </div>
</div>

<div class="container">
    <div class="card">
        <h3 class="card-header">Last updated projects</h3>
        <div class="card-block">
            <div class="card-deck">
                <c:forEach var="item" items="${projects}">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title"><a href="/projects/${item.shortName}">${item.name} &raquo;</a></h4>
                            <p class="card-text">${item.summary}</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Last updated <script>document.write(repoUpdated("${item.updated}"));</script> ago</small>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <%@include file="../templates/footer.jsp" %>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<script src="<c:url value="/resources/scripts/custom.js"/>"></script>
</body>

</html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Wojciech Pater - Contact</title>
    <%@include file="../templates/includes.jsp" %>
</head>

<body>
<!-- Menu -->
<%@include file="../templates/menu.jsp" %>
<!-- Content -->
<div class="container lower-container">
    <div class="card">
        <div class="card-block">
            <h4 class="card-title">Contact forms</h4>
            <h6 class="card-subtitle ml-1 mt-2 mb-2 text-muted">Social media</h6>
            <i class="fa fa-linkedin" aria-hidden="true"></i>
            <a class="card-link mr-2" href="https://www.linkedin.com/in/wojciech-pater-a99017103/"> Linkedin profile</a>
            <i class="fa fa-bitbucket" aria-hidden="true"></i>
            <a class="card-link mr-2" href="https://bitbucket.org/Nequeq/"> Bitbucket profile</a>
            <i class="fa fa-stack-overflow" aria-hidden="true"></i>
            <a class="card-link" href="https://stackoverflow.com/users/6490626/nequeq?tab=profile"> Stack Overflow profile</a>
            <h6 class="card-subtitle ml-1 mb-2 mt-2 text-muted">Email</h6>
            <div class="card-link"><i class="fa fa-envelope" aria-hidden="true"></i><a> wojciech.wpater@gmail.com</a>
            </div>
        </div>
    </div>
    <nav class="breadcrumb mt-4">
        <a class="breadcrumb-item" href="/">Home</a>
        <span class="breadcrumb-item active">Contact</span>
    </nav>
    <%@include file="../templates/footer.jsp" %>
</div>
</body>

</html>
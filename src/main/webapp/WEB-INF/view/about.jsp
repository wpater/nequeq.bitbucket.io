<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Wojciech Pater - Contact</title>
    <%@include file="../templates/includes.jsp" %>
</head>

<body>
<!-- Menu -->
<%@include file="../templates/menu.jsp" %>
<!-- Content -->
<div class="container lower-container">
    <div class="card mb-2">
        <div class="card-block">
            <h4 class="card-title">Studies</h4>
            <p class="card-text">I graduated from study Computer Science on Department of Computer Science, Electronics and
                Telecommunications, AGH University of Science and Technology, master's degree, grade: 4.5.</p>
            <p class="card-text">My master thesis is: <i>Big numbers library for GPU applied in cryptography.</i></p>
            <a href="projects/websym" class="btn btn-primary">Bachelor project &raquo;</a>
            <a href="projects/ulam" class="btn btn-primary">Master project &raquo;</a>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-block">
            <h4 class="card-title">Work</h4>
            <p class="card-text">
                I have worked in Itersive since 08.2018
                <span id="dateDiff"></span>
            </p>
            <p>Responsibilities:</p>
            <ul>
                <li><p>creating Intranets in offices</p></li>
                <li><p>IT helpdesk</p></li>
                <li><p>developing websites in Django CMS</p></li>
                <li><p>developing web application in Java</p></li>
                <li><p>conducting lectures</p></li>
            </ul>
            <p class="card-text">
                Previously in Comarch 11.2015 - 06.2018 (8 months).
            </p>
            <p>Responsibilities:</p>
            <ul>
                <li><p>writing new functionalities in Comarch OSS (Java)</p></li>
                <li><p>maintenance and troubleshooting in our part of Comarch OSS</p></li>
            </ul>
            <p class="card-text">
                Previously in Eriscson 09.2015 - 10.2017 (2 years and 1 month).
            </p>
            <p>Responsibilities:</p>
            <ul>
                <li><p>maintenance, troubleshooting, monitoring and improvements in automated integration tests in LTE
                    (Erlang OTP with Common Test Framework)</p></li>
                <li><p>writing new tests, help for other teams in writing tests and code review</p></li>
                <li><p>development of internal tool (Java Spring) for improving work with those tests</p></li>
                <li><p>moderation of retrospectives and weekly meetings regarding improving competences in team</p></li>
            </ul>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-block">
            <h4 class="card-title">Skills</h4>
            <h6 class="card-subtitle ml-2 mb-2 text-muted">Description</h6>
            <ul>
                <c:forEach items="${rates}" var="item">
                    <li><p class="card-text">
                        <c:forEach var="star" begin="1" end="${item.points}">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </c:forEach>
                        <c:forEach var="star" begin="${item.points}" end="4">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </c:forEach>
                        - ${item.description}
                    </p></li>
                </c:forEach>
            </ul>
            <c:forEach items="${categories}" var="category">
                <h6 class="card-subtitle ml-2 mt-2 mb-2 text-muted">${category.name}</h6>
                <div class="table-responsive">
                    <table class="table table-sm table-hover">
                        <thead class="thead-inverse">
                        <tr>
                            <%--TODO: change to w-40--%>
                            <th style="width: 40%">Name</th>
                            <th style="width: 30%">Level</th>
                            <th style="width: 15%">Experience</th>
                            <th style="width: 15%">Commercial</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${skills}" var="skill">
                            <c:if test="${skill.category eq category}">
                                <tr>
                                    <td>${skill.name}</td>
                                    <td>
                                        <c:forEach var="star" begin="1" end="${skill.rate.points}">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </c:forEach>
                                        <c:forEach var="star" begin="${skill.rate.points}" end="4">
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </c:forEach>
                                    </td>
                                    <td>${skill.experience} years</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${skill.commercial}">
                                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                            </c:when>
                                            <c:otherwise>
                                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:forEach>
        </div>
    </div>
    <nav class="breadcrumb mt-4">
        <a class="breadcrumb-item" href="/">Home</a>
        <span class="breadcrumb-item active">About me</span>
    </nav>
    <%@include file="../templates/footer.jsp" %>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/custom.js"/>"></script>
</body>

</html>
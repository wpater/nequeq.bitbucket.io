<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Wojciech Pater - Ulam on GPU</title>
    <%@include file="../templates/includes.jsp" %>
    <script>
        $('.modal').on('shown.bs.modal', function () {
            $('.modal').focus()
        })
    </script>
</head>

<body>
<!-- Menu -->
<%@include file="../templates/menu.jsp" %>

<!-- Content -->
<div class="container lower-container">
    <h1>${project.name}</h1>
    <div class="gap"></div>
    <div class="card-deck">
        <div class="card col-8">
            <div class="card-block">
                <h4 class="card-title">Description</h4>
                <p class="card-text">${project.summary}</p>
                <p class="card-text">${project.description}</p>
            </div>
        </div>
        <div class="card col-4">
            <div class="card-block">
                <h4 class="card-title">Additional info</h4>
                <p class="card-text">
                <ul>
                    <c:forEach var="item" items="${project.tags}">
                    <li><code>${item}</code></li>
                    </c:forEach>
                    <li><a href="${project.repo}">Repository</a></li>
                    <c:forEach var="item" items="${project.additional_links}">
                    <li><a href="${item.value}">${item.key}</a></li>
                    </c:forEach>
                    <c:forEach var="item" items="${project.images}">
                    <li>
                        <button type="button" class="btn btn-outline-primary btn-sm m-1" data-toggle="modal"
                                data-target="#${item.description.hashCode()}">
                                ${item.name}
                        </button>
                    </li>
                    </c:forEach>
                </p>
            </div>
        </div>
    </div>
    <nav class="breadcrumb mt-4">
        <a class="breadcrumb-item" href="/">Home</a>
        <a class="breadcrumb-item" href="/projects">My projects</a>
        <span class="breadcrumb-item active">${project.name}</span>
    </nav>
    <%@include file="../templates/footer.jsp" %>
</div>
<%--Modals--%>
<c:forEach var="item" items="${project.images}">
    <div class="modal fade" id="${item.description.hashCode()}" tabindex="-1" role="dialog"
         aria-labelledby="${item.description}" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="${item.description.hashCode()}">${item.name}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>${item.description}</p>
                    <img src="${item.path}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</c:forEach>
</body>

</html>